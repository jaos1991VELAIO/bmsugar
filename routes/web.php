<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/createInvoice','InvoiceController@generateInvoice')->name('create_invoice');
Route::post('/createInvoice','InvoiceController@createInvoice')->name('create_invoiceForm');

Route::get('/updateSequence/{id}/{type}','InvoiceController@renameContract')->name('update_sequence');


 Route::get('docs', function(){
                return View::make('docs.api.v1.index');
            });

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('testApi',function(){

	$url = 'https://bmsugar.velaio.com/api/v1/updateContract/INV-00089/k67RoSI4p1kFijJs4SDJKFY7DF634HQW5bkpfnUva5rT35hLoP';
    $client = new \GuzzleHttp\Client();
    $response = $client->get($url);

    $update_vendor = json_decode($response->getBody());

    dd($update_vendor);

});
