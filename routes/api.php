<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
	
	//Route::post('/updateContract','InvoiceController@updateContract');
	//Route::get('/updateContract',function(){
	   // \Log::info('llega por GET');
	//});
	Route::post('/updateContract','InvoiceController@updateContract');
	Route::get('/query/dispatch/{dispatch}','InvoiceController@getOneDispatch');
	//Route::get('/updateContract','InvoiceController@updateContract');
	Route::post('/updateSequence/','InvoiceController@renameContract')->name('update_sequence');

});