<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            /* Center the loader */
            #loader {
              position: absolute;
              left: 50%;
              top: 50%;
              z-index: 1;
              width: 150px;
              height: 150px;
              margin: -75px 0 0 -75px;
              border: 16px solid #f3f3f3;
              border-radius: 50%;
              border-top: 16px solid #336600;
              width: 120px;
              height: 120px;
              -webkit-animation: spin 2s linear infinite;
              animation: spin 2s linear infinite;
            }


            @-webkit-keyframes spin {
              0% { -webkit-transform: rotate(0deg); }
              100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
              0% { transform: rotate(0deg); }
              100% { transform: rotate(360deg); }
            }

            /* Add animation to "page content" */
            .animate-bottom {
              position: relative;
              -webkit-animation-name: animatebottom;
              -webkit-animation-duration: 1s;
              animation-name: animatebottom;
              animation-duration: 1s
            }

            @-webkit-keyframes animatebottom {
              from { bottom:-100px; opacity:0 } 
              to { bottom:0px; opacity:1 }
            }

            @keyframes animatebottom { 
              from{ bottom:-100px; opacity:0 } 
              to{ bottom:0; opacity:1 }
            }

            #myDiv {
              display: none;
              text-align: center;
            }

            #div_loader {
                /*position: absolute;*/
                top: 0;
                left: 0;
                width: 100%;
                min-height: 100%;
                height: auto !important;
                position: fixed;
                background: rgba(0,0,0,0.6);
                z-index: 100;
            }

        </style>
    </head>
    @if($bool_Info)
    <body>
        <div id="div_loader" style="display: none;">
            <div id="loader"></div>
        </div>
       
        
        <form action="{{ action('InvoiceController@createInvoice') }}" method="POST" id="form_create_factura">
        @csrf
        <div class="container-fluid" id="container-fluid">
            <div class="row" style="background-color: #360; padding-top: 20px;padding-bottom: 20px;">
                <div class="col-md-12"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    @if(isset($messages) && sizeof($messages) > 0)
                        @foreach($messages as $response)
                            @if($response['success'])
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  {{$response['message']}}
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                            @else
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                  {{$response['message']}}
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <label for="sales_person">Dispatch*</label>
                        <div class="col-md-12">
                            <select class="form-control" id="dispatch" name="dispatch" onchange="infoDispatch();" required="">
                                @if(isset($dispatchs) && sizeof($dispatchs) > 0)
                                    @foreach($dispatchs as $dispatch)
                                    <option value="{{$dispatch->id}}">{{$dispatch->Name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="customer_name">Customer name*</label>
                        <div class="col-md-12">
                            <select class="form-control" id="customer_name" name="customer_name" required="">
                                @if(isset($customers) && sizeof($customers) > 0)
                                    @foreach($customers as $customer)
                                    <option value="{{$customer->contact_id}}">{{$customer->contact_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <!-- <input class="form-control" type="text" name="customer_name_display" id="customer_name_display" required="" readonly="" value="">
                            <input class="form-control" type="hidden" name="customer_name_two" id="customer_name_two" required="" value=""> -->
                        </div>
                    </div>
                   <!-- <br>
                    <div class="row">
                        <label for="invoice_number">Invoice #*</label>
                        <div class="col-md-12">
                            <input class="form-control" type="text" name="invoice_number" id="invoice_number" required="">
                        </div>
                    </div>-->
                    <br>
                    <div class="row">
                        <label for="order_number">Order Number</label>
                        <div class="col-md-12">
                            <input class="form-control" type="text" name="order_number" id="order_number">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="invoice_date">Invoice Date*</label>
                        <div class="col-md-12">
                            <input class="form-control" type="text" name="invoice_date" id="invoice_date" required="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="terms">Terms</label>
                        <div class="col-md-12">
                            <input class="form-control" type="text" name="terms" id="terms">
                        </div>
                    </div>
                    <!--<br>
                    <div class="row">
                        <label for="due_date">Due Date</label>
                        <div class="col-md-12">
                            <input class="form-control" type="text" name="due_date" id="due_date" required="">
                        </div>
                    </div>-->
                    <br>
                    <div class="row">
                        <label for="sales_person">Salesperson</label>
                        <div class="col-md-12">
                            <input class="form-control" type="text" name="sales_person" id="sales_person">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="sales_person">Apply pre-invoice</label>
                        <div class="col-md-12 text-center">
                            <input class="form-control" type="checkbox" name="apply_pre_invoice" id="apply_pre_invoice" style="height: 40px;" onclick="preInvoice()">
                        </div>
                        <input type="hidden" name="boolPreInvoice" id="boolPreInvoice" value="false">
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" id="div_pre_invoice" style="display:none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="sales_person">Amount pre-invoice</label>
                                    <input class="form-control" type="number" name="amount_pre_invoice" id="amount_pre_invoice">
                                </div>
                                <div class="col-md-6">
                                    <label for="notes_pre_invoice">Notes pre-invoice</label>
                                    <textarea rows="4" cols="50" class="form-control" name="notes_pre_invoice" id="notes_pre_invoice"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover" id="items_factura">
                                <thead>
                                    <tr>
                                        <th>
                                            Action
                                        </th>
                                        <th>
                                            Item Details
                                        </th>
                                        <th>
                                            Quantity
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Tax (%)
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="table-active">
                                        <td>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="select_item_1" id="select_item_1" value="{{$dispatchs[0]->Type_of_product}}">
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="quantity_1" id="quantity_1" value="{{$dispatchs[0]->Metrics_tons}}" onchange="reCalculateAmount();" readonly="">
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="rate_1" id="rate_1" value="{{$dispatchs[0]->info_contract->Selling_price}}" onchange="reCalculateAmount();" readonly="">
                                        </td>
                                        <td>
                                            <input class="form-control" type="number" name="tax_1" id="tax_1" value="0" min="0" max="100" onchange="reCalculateAmount();">%
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="amount_1" id="amount_1" value="{{$dispatchs[0]->Metrics_tons*$dispatchs[0]->info_contract->Selling_price}}" onchange="reCalculateSubtotal();" readonly="">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <a onclick="addRow();" style="cursor:pointer;">Add item</a>
                        </div>
                    </div>
                    <input type="hidden" name="num_items" id="num_items" value="1">
                    <input type="hidden" name="num_items_tab" id="num_items_tab" value="1">
                    <input type="hidden" name="name_contract" id="name_contract" value="{{$dispatchs[0]->info_contract->Name}}">
                    <input type="hidden" name="id_contract" id="id_contract" value="{{$dispatchs[0]->info_contract->id}}">
                    <input type="hidden" name="name_dispatch" id="name_dispatch" value="{{$dispatchs[0]->Name}}">
                    <input type="hidden" name="id_dispatch" id="id_dispatch" value="{{$dispatchs[0]->id}}">
                    <input type="hidden" name="business_type" id="business_type" value="{{$dispatchs[0]->Company}}">
                    @if(isset($dispatchs[0]->ingenuity['verify_stock']['sum']))
                    <input type="hidden" name="stock" id="stock" value="{{$dispatchs[0]->ingenuity['verify_stock']['sum']}}">
                    @else
                    <input type="hidden" name="stock" id="stock" value="">
                    @endif
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">Subtotal</div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="subtotal" id="subtotal" value="0" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">Discount</div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-6" style="padding-right: 0px !important;">
                                                    <input class="form-control" type="number" name="discount_val" id="discount_val" value="0" min="0" onchange="reCalculateTotal();">
                                                </div>
                                                <div class="col-md-6" style="padding-left: 0px !important;">
                                                    <select id="discount_type" name="discount_type" class="form-control" >
                                                        <option value="0" selected="">$</option>
                                                        <option value="1">%</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control" name="discount" id="discount" value="0" min="0" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">Shipping Charges</div>
                                        <div class="col-md-4">
                                            <input class="form-control" type="number" name="shipping_charges_val" id="shipping_charges_val" min="0" value="0" onchange="reCalculateTotal();">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control" name="shipping_charges" id="shipping_charges" min="0" value="0" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">Adjustment</div>
                                        <div class="col-md-4">
                                            <input class="form-control" type="number" name="adjustment_val" id="adjustment_val" min="0" value="0" onchange="reCalculateTotal();">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control" name="adjustment" id="adjustment" min="0" value="0" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">Total($)</div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control" name="total" id="total" value="0" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            Customer Notes
                                            <input class="form-control" type="text" name="customer_notes" id="customer_notes">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            Terms & Conditions
                                            <input class="form-control" type="text" name="terms_conditions" id="terms_conditions">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info" disabled="" id="boton_submit">Crear Factura</button>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
                <div class="col-md-1">
                </div>
            </div>
        </div>
        </form>
    </body>
    @else
    <body>
        <div class="col-md-12 text-center">
            <br><br>
            <p>No existen despachos disponibles para generar una factura</p>
        </div>
    </body>
    @endif

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
<!-- <script
  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script> -->


<script type="text/javascript">

    window.onload = function() {
      reCalculateAmount();
    };

    $('#invoice_date').datepicker({
        uiLibrary: 'bootstrap4'
    });

    $('#due_date').datepicker({
        uiLibrary: 'bootstrap4'
    });

    function preInvoice(){

        var bool = document.getElementById("boolPreInvoice");
        var div = document.getElementById("div_pre_invoice");

        if (bool.value == 'false') {
            div.style.display="block";
            bool.value = "true";
        }else{
            div.style.display="none";
            bool.value = "false";
        }

    }

    function verifyStock(){
        var cant = document.getElementById("quantity_1").value;
        var stock = document.getElementById("stock").value;
        console.log(cant);
        console.log(stock);

        if (stock == null || stock == 'null' || stock == '') {
            console.log('1-stock');
            alert('No se tiene relacionados los ingenios de los cuales se tomara el stock de productos.');
        }

        if (cant*1 > stock*1) {
            console.log('2-stock');
            alert('No se puede ejecutar la facturaci贸n por falta de producto en stock.');
        }

        if (cant*1 <= stock*1) {
            console.log('3-stock');
            document.getElementById('boton_submit').disabled = false;
        }

    }

    function addRow(){

        var last_item = parseInt(document.getElementById("num_items").value);
        var last_item_tab = parseInt(document.getElementById("num_items_tab").value);
        var new_item = last_item+1;
        var new_item_tab = last_item_tab+1;

        var table = document.getElementById("items_factura");
        var row = table.insertRow(new_item_tab);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);

        cell1.innerHTML = "<a onclick='deleteRow("+new_item+");' style='cursor:pointer;'>Borrar</a>";
        cell2.innerHTML = "<input class='form-control' type='text' name='select_item_"+ new_item +"' id='select_item_"+ new_item +"' value='{{$dispatchs[0]->Type_of_product}}'>";
        cell3.innerHTML = "<input class='form-control' type='text' name='quantity_"+ new_item +"' id='quantity_"+ new_item +"' value='{{$dispatchs[0]->Metrics_tons}}' onchange='reCalculateAmount();'>";
        cell4.innerHTML = "<input class='form-control' type='text' name='rate_"+ new_item +"' id='rate_"+ new_item +"' value='{{number_format($dispatchs[0]->info_contract->Selling_price)}}' onchange='reCalculateAmount();'>";
        cell5.innerHTML = "<input class='form-control' type='number' name='tax_"+ new_item +"' id='tax_"+ new_item +"' value='0' min='0' max='100' onchange='reCalculateAmount();'>%";
        cell6.innerHTML = "<input class='form-control' type='text' name='amount_"+ new_item +"' id='amount_"+ new_item +"' value='{{number_format($dispatchs[0]->Metrics_tons*$dispatchs[0]->info_contract->Selling_price)}}' onchange='reCalculateSubtotal();' readonly=''>";

        document.getElementById("num_items").value = new_item;
        document.getElementById("num_items_tab").value = new_item_tab;

        reCalculateAmount();

    }

    function deleteRow(num){
        document.getElementById("items_factura").deleteRow(num);

        var last_item = parseInt(document.getElementById("num_items_tab").value);
        document.getElementById("num_items_tab").value = last_item-1;

        reCalculateAmount();
    }

    function reCalculateAmount(){

        verifyStock();

        var num_items = document.getElementById("num_items").value;

        for (var i = 1; i <= num_items; i++) {

            if (document.getElementById("quantity_"+i)){

                var num = i;

                var quantity = parseInt(document.getElementById("quantity_"+num).value);

                var rate_tmp = document.getElementById("rate_"+num).value;
                var rate = parseInt(rate_tmp.replace(",", ""));

                var tax = parseInt(document.getElementById("tax_"+num).value);

                console.log(quantity);
                console.log(rate);
                console.log(tax);

                var amount_tmp = quantity*rate;

                console.log(amount_tmp);

                if (tax > 0) {
                    console.log('si es mayor a cero el TAX');
                    amount_tmp = Math.ceil(amount_tmp + (amount_tmp*(tax/100)));
                    console.log(amount_tmp);
                }

                //var amount = new Intl.NumberFormat("es-ES").format(amount_tmp);
                var amount = amount_tmp;
                console.log(amount);

                document.getElementById("amount_"+num).value = amount;
            }
        }

        reCalculateSubtotal();
    }

    function reCalculateSubtotal(){

        var num_items = document.getElementById("num_items").value;
        var amount_tmp = 0;

        for (var i = 1; i <= num_items; i++) {

            if (document.getElementById("amount_"+i)){

                console.log(i);
                var value_tmp = document.getElementById("amount_"+i).value;
                amount_tmp = amount_tmp + parseFloat(value_tmp);

            }
        }

        //var amount = new Intl.NumberFormat("es-ES").format(amount_tmp);
        var amount = amount_tmp;
        console.log(amount);

        document.getElementById("subtotal").value = amount;

        reCalculateTotal();

    }

    function reCalculateTotal(){
        console.log('calculate total');
        var subtotal = document.getElementById("subtotal").value;

        var discount_val = document.getElementById("discount_val").value;
        var discount_type = document.getElementById("discount_type").value;
        var discount = document.getElementById("discount").value;

        var shipping_charges_val = document.getElementById("shipping_charges_val").value;
        var shipping_charges = document.getElementById("shipping_charges").value;

        var adjustement_val = document.getElementById("adjustment_val").value;
        var adjustement = document.getElementById("adjustment").value;

        var total = document.getElementById("total").value;

        var amount_tmp = parseInt(subtotal);
        console.log(amount_tmp);
        //inicio descuento
        if (discount_type == 0) {//$

            var discount_tmp = parseInt(discount_val.replace(".", ""));

            amount_tmp = amount_tmp - discount_tmp;

        }else if(discount_type == 1){//%

            var discount_tmp = amount_tmp * parseInt(discount_val.replace(".", ""));

            amount_tmp = amount_tmp - discount_tmp;

        }

        //document.getElementById("discount").value = new Intl.NumberFormat("es-ES").format(Math.ceil(discount_tmp));
        document.getElementById("discount").value = Math.ceil(discount_tmp);

        //Fin descuento

        //cargos adicionales

        amount_tmp = amount_tmp + parseInt(shipping_charges_val.replace(".", "")) + parseInt(adjustement_val.replace(".", ""));

        //shipping_charges = new Intl.NumberFormat("es-ES").format(Math.ceil(shipping_charges_val));
        //adjustement = new Intl.NumberFormat("es-ES").format(Math.ceil(adjustement_val));

        shipping_charges = Math.ceil(shipping_charges_val);
        adjustement = Math.ceil(adjustement_val);
        

        //Fin cargos adicionales
        console.log(amount_tmp);
        //var amount = new Intl.NumberFormat("es-ES").format(amount_tmp);
        var amount = amount_tmp;
        console.log(amount);

        document.getElementById("total").value = amount;

    }

    function infoDispatch(){
        var e = document.getElementById("dispatch");
        var dispatch = e.options[e.selectedIndex].value;

        showPage();

        $.ajax({
            type: "GET",
            contentType: "application/x-www-form-urlencoded",
            url: "{{env('APP_URL')}}/api/v1/query/dispatch/"+dispatch,
            success: function (data){
                console.log(data);
                //update table invoice
                document.getElementById("select_item_1").value = data[0].Type_of_product;
                document.getElementById("quantity_1").value = data[0].Metrics_tons;
                document.getElementById("rate_1").value = data[0].info_contract.Selling_price;

                //update info contract
                document.getElementById("name_contract").value = data[0].info_contract.Name;
                document.getElementById("id_contract").value = data[0].info_contract.id;
                document.getElementById("name_dispatch").value = data[0].Name;
                document.getElementById("id_dispatch").value = data[0].id;
                document.getElementById("business_type").value = data[0].Company;
                document.getElementById("stock").value = 10000;

                //Customer
                //document.getElementById("customer_name_display").value = data[0].Account_name.name;
                //document.getElementById("customer_name").value = data[0].Account_name.id;

                reCalculateAmount();

                hiddePage();
            },
            error: function (data){ console.log(data); }
        });
    }

    function showPage() {
      document.getElementById("div_loader").style.display = "block";
      //document.getElementById("container-fluid").style.display = "block";
    }

    function hiddePage() {
      document.getElementById("div_loader").style.display = "none";
      //document.getElementById("container-fluid").style.display = "none";
    }
</script>

</html>
