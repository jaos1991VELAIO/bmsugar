<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class InvoiceController extends Controller
{
    /**
    * Este servicio permite actualizar la informacion de contratos en una factura.
    * 
    * @param string $factura Nombre: Factura a la cual se le ejecutara la operacion de actualizacion del contrato.
    * @param string $token Nombre: Token de autenticacion con el api de servicios.
    *
    * @return Single Response
    */
    public function updateContract(Request $request){

        //dd($this->historyData('ING - 220332',1));

        \Log::info($request->all());
        //$msj = "Unable to process the request for integrity in the data received.";
        //return $this->jsonResponse(false, "Contract update operation", $msj,0,null,400); 

        if (!empty($request->get('factura')) && !empty($request->get('token'))) {

            $factura_ws = $request->get('factura');
            $token_ws = $request->get('token');

            if ($token_ws == env('TOKEN_API')) {
                \Log::info('Se recibe el numero de factura');
                $organizations = $this->getOrganization(env('TOKEN_ZOHO_INVENTORY')); 
                
                if (!empty($organizations)) {
                    \Log::info('Se encuentra organizacion');

                    $getInvoice = $this->getInvoices(env('TOKEN_ZOHO_INVENTORY'),$organizations->organization_id,$factura_ws);

                    if (!empty($getInvoice)) {
                        \Log::info('Se obtiene una factura');

                        $invoice_contract = explode("/", $getInvoice->custom_field_hash->cf_client_contract_id);
                        $invoice_dispatch = explode("/", $getInvoice->custom_field_hash->cf_dispatch_name);

                        $info = array(
                            'name_contract' => $invoice_contract[0],
                            'id_contract' => $invoice_contract[1],
                            'name_dispatch' => $invoice_dispatch[0],
                            'id_dispatch' => $invoice_dispatch[1]
                        );

                        //Consultar dispacth
                        $dispatch = $this->getOneDispatch($info['id_dispatch']);

                        //Consulta tracking despachos
                        $vendor_contracts = $this->getVendorContractsDispatch($info['id_dispatch']);

                        $info_return = [];

                        //1° Update contract clients
                        $updateContractClient = $this->updateContractClient($info['name_contract'],$info['id_contract'],$info['name_dispatch'],$info['id_dispatch'],$factura_ws,$dispatch->Metrics_tons,$vendor_contracts,$getInvoice);

                        $info_return[] = $updateContractClient; 
                        $id_contracts_vendor = [];

                        //2° Update Contracts Vendors
                        foreach ($vendor_contracts['contracts'] as $key_up => $value_up) {
                            $id_contracts_vendor[] = $value_up->id;
                            $updateContractVendor = $this->updateContractVendor($value_up->name,$value_up->id,$info['name_dispatch'],$info['id_dispatch'],$factura_ws,$dispatch->Metrics_tons,$vendor_contracts,$getInvoice);

                            $info_return[] = $updateContractVendor; 
                        }

                        $msj = "No se encontró una factura relacionada.";
                        return $this->jsonResponse(true, "Operación de actualizacion de contrato", $msj,5,$info_return,200); 

                    }else{
                       $msj = "No se encontró una factura relacionada.";
                        return $this->jsonResponse(false, "Operación de actualizacion de contrato", $msj,4,null,200); 
                    }

                }else{
                    $msj = "No se encontró una organización relacionada al token Inventory propocionado.";
                    return $this->jsonResponse(false, "Operación de actualizacion de contrato", $msj,3,null,404);
                }
            }else{
                $msj = "El token proporcionado no corresponde al autorizado, verifique y reintente.";
                return $this->jsonResponse(false, "Operación de actualizacion de contrato", $msj,2,null,404);
            }
        }else{
            $msj = "No se recibio un numero de factura para iniciar la operación, por favor ingreselo e intente nuevamente.";
            return $this->jsonResponse(false, "Operación de actualizacion de contrato", $msj,1,null,200);
        }
    }

    public function historyData($contract_name,$type){

        $history = [];

        if($type == 1){ //history vendor

            $url = env('UPDATE_TRACKING_CONTRATO_VENDOR');
            $client = new \GuzzleHttp\Client();
            $response = $client->get($url, ['json' => null,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);

            $history_vendor = json_decode($response->getBody());
            //dd($history_vendor);
            if (isset($history_vendor->data) && sizeof($history_vendor->data) > 0) {

                foreach ($history_vendor->data as $key_history_vendor => $value_history_vendor) {

                    if (!empty($value_history_vendor->Parent_Id) && $value_history_vendor->Parent_Id->name == $contract_name) {
                        $history[]= array(
                            'Date' => $value_history_vendor->Date,
                            'Invoice_number' => $value_history_vendor->Invoice_number,
                            'Invoiced' => $value_history_vendor->Invoiced,
                            'Dispatch_ID' => $value_history_vendor->Dispatch_ID->id,
                            'Tons' => $value_history_vendor->Tons,
                            'URL_1' => $value_history_vendor->URL_1
                        );
                    }
                }

                if (sizeof($history) > 0) {
                    return $history;
                }else{
                    return null;
                }
                
            }

            

        }elseif ($type == 2) { //history client
            
            $url = env('UPDATE_TRACKING_CONTRATO_CLIENT');
            $client = new \GuzzleHttp\Client();
            $response = $client->get($url, ['json' => null,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);
            
            $history_client = json_decode($response->getBody());

            if (isset($history_client->data) && sizeof($history_client->data) > 0) {

                foreach ($history_client->data as $key_history_client => $value_history_client) {
                    if (!empty($value_history_client->Parent_Id) && $value_history_client->Parent_Id->name == $contract_name) {
                        $history[]= array(
                            'Date' => $value_history_client->Date,
                            'Invoice_ID' => $value_history_client->Invoice_ID,
                            'Invoiced' => $value_history_client->Invoiced,
                            'Dispatchs' => $value_history_client->Dispatchs->id,
                            'Tons' => $value_history_client->Tons,
                            'Invoice_link' => $value_history_client->Invoice_link
                        );
                    }
                }

                if (sizeof($history) > 0) {
                    return $history;
                }else{
                    return null;
                }
            }

        }

        return null;
    }

    public function getOrganization($token){

        $url = env('CONSULTAR_ORGANIZACIONES')."?authtoken=".$token;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url);
        $organization = json_decode($request->getBody());

        if (isset($organization->message) && $organization->message == 'success') {
            
            if(!empty(env('DEFAULT_ORGANIZATION'))){
                foreach($organization->organizations as $key_org => $value_org){
                    
                    if($value_org->organization_id == env('DEFAULT_ORGANIZATION')){
                        return $organization->organizations[$key_org];
                    }
                }
            }
            
            return $organization->organizations[0];
        }else{
            return null;
        }
    }

    public function getInvoices($token,$organization_id,$factura){

        $url = env('CONSULTAR_FACTURAS')."?authtoken=".$token."&organization_id=".$organization_id;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url);
        $invoices = json_decode($request->getBody());

        if (isset($invoices->message) && $invoices->message == 'success') {
            foreach ($invoices->invoices as $key_invoice => $value_invoice) {
                if ($value_invoice->invoice_number == $factura) {
                    return $this->getInvoice($token,$organization_id,$value_invoice->invoice_id);
                    break;
                }
            }

        }else{
            return null;
        }
    }

    public function getInvoice($token,$organization_id,$factura_id){

        $url = env('CONSUTAR_FACTURA')."/".$factura_id."?authtoken=".$token."&organization_id=".$organization_id;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url);
        $invoice = json_decode($request->getBody());

        if (isset($invoice->message) && $invoice->message == 'success') {

            return $invoice->invoice;

        }else{
            return null;
        }
    }

    public function getVendorContract($search){

        //Limpiamos el valor a buscar, solo sucede con este vendor name
        $search_ = str_replace(" ", "", $search);
        //$search = str_replace("-", " - ", $search_);

        $vendor_contract = null;
        $url = env('CRUD_CONTRATO_VENDOR');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
        $vendor_contracts = json_decode($request->getBody());

        if (isset($vendor_contracts->data) && sizeof($vendor_contracts->data) > 0) {

            foreach ($vendor_contracts->data as $key_contract => $value_contract) {
                if ($value_contract->Name == $search) {
                    $vendor_contract = $value_contract;
                }
            }

        }
        
        return $vendor_contract;
    }

    public function getClientContract($search){

        //Limpiamos el valor a buscar, solo sucede con este vendor name
        $search = str_replace(" ", "", $search);

        $client_contract = null;
        $url = env('CRUD_CONTRATO_CLIENT');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
        $client_contracts = json_decode($request->getBody());

        if (isset($client_contracts->data) && sizeof($client_contracts->data) > 0) {
            
            foreach ($client_contracts->data as $key_contract => $value_contract) {
                if ($value_contract->Name == $search) {
                    $client_contract = $value_contract;
                }
            }

        }
        
        return $client_contract;
    }

    public function getDetailItems($token,$organization_id,$item_id){
        $url = env('CONSULTAR_ITEMS_FACTURA')."/".$item_id."?authtoken=".$token."&organization_id=".$organization_id;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url);
        $item = json_decode($request->getBody());

        if (isset($item->message) && $item->message == 'success') {

            return $item->item;

        }else{
            return null;
        }
    }

    public function isUpdate($data,$type){
        \Log::info('se ingresa a verificar si es o no una factura ya procesada y si se va a actualizar');
        $isUpdate = false;
        $id = null;
        $evaluate = "";
        $oper_tons = null;
        $cant_tons = null;
        $oper_invoiced = null;
        $cant_invoiced = null;
        $Update = false;
        $new_cant_tons = null;
        $new_cant_invoiced = null;

        if ($type == 1) {
            //tracking invoice vendor

            $url = env('UPDATE_TRACKING_CONTRATO_VENDOR');
            $client = new \GuzzleHttp\Client();
            $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
            $tracking_vendor = json_decode($request->getBody());

            if (!empty($tracking_vendor) && sizeof($tracking_vendor->data) > 0) {
                foreach ($tracking_vendor->data as $key_vendor => $value_vendor) {
                    if ($value_vendor->Invoice_number == $data['Invoice_number'] && $value_vendor->Dispatch_ID == $data['Dispatch_ID']) {
                        
                        $isUpdate = true;
                        $id = $value_vendor->id;
                        $evaluate = $value_vendor;
                        break;

                    }
                }
            }

        }elseif ($type == 2) {
            //tracking invoice client

            //Limpiamos el valor a buscar, solo sucede con este vendor name
            $data_ = str_replace(" ", "", $data);

            $url = env('UPDATE_TRACKING_CONTRATO_CLIENT');
            $client = new \GuzzleHttp\Client();
            $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
            $tracking_client = json_decode($request->getBody());

            if (!empty($tracking_client) && sizeof($tracking_client->data) > 0) {
                foreach ($tracking_client->data as $key_client => $value_client) {
                    if ($value_client->Invoice_ID == $data['Invoice_ID'] && $value_client->Dispatchs == $data['Dispatchs']) {
                        
                        $isUpdate = true;
                        $id = $value_client->id;
                        $evaluate = $value_client;
                        break;

                    }
                }
            }
        }


        if ($evaluate != "") {
            
            if ($evaluate->Tons != $data['Tons']) {
                $Update = true;
                //Haga esto
                if ($data['Tons'] > $evaluate->Tons) {
                    $oper_tons = 'res';
                    $cant_tons = $data['Tons'] - (int)$evaluate->Tons;
                    $new_cant_tons = (int)$data['Tons'];
                }elseif ($data['Tons'] < $evaluate->Tons) {
                    $oper_tons = 'sum';
                    $cant_tons = (int)$evaluate->Tons - $data['Tons'];
                    $new_cant_tons = (int)$data['Tons'];
                }
            }

            if ($evaluate->Invoiced != $data['Invoiced']) {
                $Update = true;
                //Haga esto
                if ($data['Invoiced'] > $evaluate->Invoiced) {
                    $oper_invoiced = 'sum';
                    $cant_invoiced = (int)$data['Invoiced'] - $evaluate->Invoiced;
                    $new_cant_invoiced = (int)$data['Invoiced'];
                }elseif ($data['Invoiced'] < $evaluate->Invoiced) {
                    $oper_invoiced = 'res';
                    $cant_invoiced = (int)$evaluate->Invoiced - $data['Invoiced'];
                    $new_cant_invoiced = (int)$data['Invoiced'];
                }
            }

        }

        \Log::info(array('isUpdate' => $isUpdate,'Update' => $Update,'id' => $id,'data' => array('oper_tons'=>$oper_tons, 'cant_tons' => $cant_tons,'oper_invoiced' => $oper_invoiced,'cant_invoiced' => $cant_invoiced)));

        return array('isUpdate' => $isUpdate,'Update' => $Update,'id' => $id,'data' => array('oper_tons'=>$oper_tons, 'cant_tons' => $cant_tons,'oper_invoiced' => $oper_invoiced,'cant_invoiced' => $cant_invoiced, 'new_cant_tons' => $new_cant_tons, 'new_cant_invoiced' => $new_cant_invoiced));
    }

    public function createInvoice(Request $request){

        if ($request->get('business_type') == 'US') {
                $prefix = 'PrefixUS';
                $value = 'ValueUS';
        }else{
                $prefix = 'PrefixEU';
                $value = 'ValueEU';
        }
        
        $settings = $this->globalOneSettings('invoice',$prefix,$value);


        $customer_id = $request->get('customer_name');
        $invoice_number = $settings[$prefix].$settings[$value];

        //date
        $date_tmp = explode("/", $request->get('invoice_date'));
        $date = $date_tmp[2]."-".$date_tmp[0]."-".$date_tmp[1];

        //Due date
        //$due_date_tmp = explode("/", $request->get('due_date'));
        //$due_date = $due_date_tmp[2]."-".$due_date_tmp[0]."-".$due_date_tmp[1];
        $due_date = Carbon::now()->addDays(3)->format('Y-m-d');

        $payment_terms = "15";
        $payment_terms_label = "Net 15";

        //General discount
        if ($request->get('discount_type') == 1) { //%
            $general_discount = $request->get('discount_val').'%';
        }elseif ($request->get('discount_type') == 0) {//$
            $general_discount = $request->get('discount_val');
        }else{
            $general_discount = 0;
        }

        //shipping_charges
        $shipping_charges = ($request->get('shipping_charges_val') !== null && !empty($request->get('shipping_charges_val'))) ? $request->get('shipping_charges_val') : 0;
        
        //adjustment
        $adjustment = ($request->get('adjustment_val') !== null && !empty($request->get('adjustment_val'))) ? $request->get('adjustment_val') : 0;

        $items = [];

        for ($i=1; $i <= (int)$request->get('num_items') ; $i++) { 
            
            $select_item = "select_item_".$i;
            $quantity = "quantity_".$i;
            $rate = "rate_".$i;
            $tax = "tax_".$i;

            if ($request->get($select_item) !== null) {

                $rate_clean_tmp = str_replace(".", "", $request->get($rate));
                $rate_clean = str_replace(",", "", $rate_clean_tmp);
                
                $items[] = array(
                    'name' => $request->get($select_item),
                    'quantity' => $request->get($quantity),
                    'rate' => $rate_clean,
                    'tax_percentage' => $request->get($tax)
                    );

            }

        }

        //info para updates
        $name_contract = $request->get('name_contract');
        $id_contract = $request->get('id_contract');
        $name_dispatch = $request->get('name_dispatch');
        $id_dispatch = $request->get('id_dispatch');
        $quantity_1 = $request->get('quantity_1');
        $amount_1 = $request->get('amount_1');
        $terms = ($request->get('customer_notes')) ? $request->get('customer_notes') : null;
        $notes = ($request->get('terms_conditions')) ? $request->get('terms_conditions') : null;
        $template = ($request->get('business_type') == 'US') ? '1671282000000017001' : '1671282000000283001';

        $send_tmp = array(
                    'customer_id' => $customer_id,
                    'invoice_number' => $invoice_number,
                    'date' => $date,
                    'due_date' => $due_date,
                    'payment_terms' => $payment_terms,
                    'payment_terms_label' => $payment_terms_label,
                    "custom_fields" => [
                        [
                            "label" => "Dispatch ID",
                            "value" => $name_dispatch."/".$id_dispatch
                        ],
                        [
                            "label" => "Client contract id",
                            "value" => $name_contract."/".$id_contract
                        ],
                        [
                            "label" => "Apply pre-invoice",
                            "value" => ($request->get('boolPreInvoice') == 'true') ? true: false,
                        ],
                        [
                            "label" => "Amount pre-invoice",
                            "value" => ($request->get('boolPreInvoice') == 'true') ? $request->get('amount_pre_invoice'): null,
                        ],
                        [
                            "label" => "Note pre-invoice",
                            "value" => ($request->get('boolPreInvoice') == 'true') ? $request->get('notes_pre_invoice'): null,
                        ]
                    ],
                    'discount' => $general_discount,
                    'shipping_charge' => $shipping_charges,
                    'shipping_charge' => $shipping_charges,
                    'adjustment' => $adjustment,
                    'viewed' => 'viewed',
                    'terms' => $terms,
                    'notes' => $notes,
                    'template_id' => $template,
                    'line_items' => $items
                );

        $send = json_encode($send_tmp);

        $url = env('INVOICES')."/?authtoken=".env('TOKEN_ZOHO_INVENTORY')."&organization_id=".env('ORGANIZATION_INVOICES');
        $client = new \GuzzleHttp\Client();
        $request = $client->request('POST', $url, ['form_params' => ['JSONString' => $send],'headers' => [
                                        'Content-Type'     => 'application/x-www-form-urlencoded',
                                        'Authorization'      => env('TOKEN_ZOHO_INVENTORY')
                                    ]]);
        $invoice = json_decode($request->getBody());

        if ($invoice->code !== null && $invoice->code == 0) {
            
            $updateGlobal = $this->globalUpdateSettings('invoice',$value,$settings);

            $info = array(
                    'name_contract' => $name_contract,
                    'id_contract' => $id_contract,
                    'name_dispatch' => $name_dispatch,
                    'id_dispatch' => $id_dispatch
                );

            $response = $this->updateContractsDispatchs($info,$invoice_number,$quantity_1,$id_dispatch,$amount_1);

            //redireccionar
            $customers = $this->getContacts(env('TOKEN_ZOHO_INVENTORY'),env('ORGANIZATION_INVOICES'));
            $dispatchs = $this->getDispatchs(env('TOKEN_ZOHO_CRM'));

            if (sizeof($dispatchs) > 0) {
                $bool_Info = true;
                $dispatchs_json = json_encode($dispatchs);
            }else{
                $bool_Info = false;
                $dispatchs_json = null;
            }
            
            //dd($customers,$dispatchs);
            return view('createInvoice')
                ->with('bool_Info',$bool_Info)
                ->with('customers',$customers)
                ->with('dispatchs',$dispatchs)
                ->with('dispatchs_json',$dispatchs_json)
                ->with('messages',$response);

        }else{

            $response = array('success' => false, 'message' => $invoice->message);

            //redireccionar
            $customers = $this->getContacts(env('TOKEN_ZOHO_INVENTORY'),env('ORGANIZATION_INVOICES'));
            $dispatchs = $this->getDispatchs(env('TOKEN_ZOHO_CRM'));
            
            if (sizeof($dispatchs) > 0) {
                $bool_Info = true;
                $dispatchs_json = json_encode($dispatchs);
            }else{
                $bool_Info = false;
                $dispatchs_json = null;
            }
            
            //dd($customers,$dispatchs);
            return view('createInvoice')
                ->with('bool_Info',$bool_Info)
                ->with('customers',$customers)
                ->with('dispatchs',$dispatchs)
                ->with('dispatchs_json',$dispatchs_json)
                ->with('messages',$response);
        }
    }

    public function updateContractsDispatchs($info,$factura,$tons,$id_dispatch,$amount_1){

        $info_return = [];

        //consulta de factura
        $getInvoice = $this->getInvoices(env('TOKEN_ZOHO_INVENTORY'),env('ORGANIZATION_INVOICES'),$factura);

        if (empty($getInvoice)) {
            return array('success' => false, 'message' => 'No se pudo obtener informacion de la factura (Sección actualizacion de client contract)');
        }

        //consultar informacion de despacho
        $dispatch = $this->getOneDispatch($id_dispatch);

        //Consulta tracking despachos
        $vendor_contracts = $this->getVendorContractsDispatch($id_dispatch);

        if (empty($vendor_contracts['names_contracts']) && $vendor_contracts['names_contracts'] < 0) {
            return array('success' => false, 'message' => 'No se pudo obtener informacion de los contratos relacionados en el despacho (Sección actualizacion de contratos)');
        }

        //1° Update contract clients
        $updateContractClient = $this->updateContractClient($info['name_contract'],$info['id_contract'],$info['name_dispatch'],$info['id_dispatch'],$factura,$tons,$vendor_contracts,$getInvoice);

        $info_return[] = $updateContractClient; 
        $id_contracts_vendor = [];

        //2° Update Contracts Vendors
        foreach ($vendor_contracts['contracts'] as $key_up => $value_up) {
            $id_contracts_vendor[] = $value_up->id;
            $updateContractVendor = $this->updateContractVendor($value_up->name,$value_up->id,$info['name_dispatch'],$info['id_dispatch'],$factura,$tons,$vendor_contracts,$getInvoice);

            $info_return[] = $updateContractVendor; 
        }

        //3º Insert de informacion en cartera
        $payments = $this->insertPayment($getInvoice,$tons,$vendor_contracts,$info,$dispatch,$id_contracts_vendor,$amount_1);

        $info_return[] = $payments; 

        return $info_return;   
    }

    public function updateContractClient($name_contract,$id_contract,$name_dispatch,$id_dispatch,$factura,$tons,$vendor_contracts,$getInvoice){

        $billed_value = $getInvoice->total; //Valor facturado
        $checked_date = $getInvoice->created_time; //Fecha de facturacion
        $invoice_url = "https://invoice.zoho.com/app#/invoices/".$getInvoice->invoice_id; //Url de la factura
        $summary_invoice_items = $getInvoice->line_items;//Resumen de productos facturados
        $tons = $tons;
        $date = explode("T",$getInvoice->created_time);

        $contract_client = $this->getClientContract($name_contract);

        if (empty($contract_client)) {
            return array('success' => false, 'message' => 'No se pudo obtener informacion del contrato (Sección actualizacion de client contract)'.$name_contract);
        }

        if ($contract_client->Status == "Not started" && (int)$contract_client->Remain_balance == 0 || $contract_client->Status == "Not started" && $contract_client->Remain_balance == null) {
                                
            $data_client_contract_ = array(
                    'Remain_balance' => (int)$contract_client->Tons_number
                );
        
            $client_contract_ = [
                    'data' => [$data_client_contract_]
                ];

            $url = env('CRUD_CONTRATO_CLIENT')."/".$contract_client->id;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $client_contract_,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);

            $update_contract_client_ = json_decode($response->getBody());

            $contract_client = $this->getClientContract($name_contract);//Actualizo la informacion
            
        }

        //Insert o update en tracking invoice client
        $data_client_tracking = array(
            'Date' => $date[0],
            'Invoice_number' => $getInvoice->invoice_number,
            'Invoiced' => (string)$billed_value,
            'Dispatch_ID' => $id_dispatch,
            'Vendor_contract_IDs' => $name_contract,
            'Tons' => (string)$tons,
            'URL_1' => $invoice_url
        );

        $tmp_client_tracking = array('id' => $id_contract, 'Tracking_invoices' => [$data_client_tracking]);

        $client_tracking = [
            'data' => [$tmp_client_tracking]
        ];

        \Log::info((array)$client_tracking);

        $verify_client_tracking = $this->isUpdate($data_client_tracking,1);

        //Validacion para continuar o finalizar el proceso
        if ($verify_client_tracking['isUpdate'] == true && $verify_client_tracking['Update'] == false) {
                \Log::info('Es una actualizacion, pero no se encontraron cambios en la factura.');
                $msj = "No se encontraron actualizaciones para la factura ".$getInvoice->invoice_number;
                return $this->jsonResponse(false, "Operación de actualizacion de contrato", $msj,9,null,200); 
        }

        if ($verify_client_tracking['Update']) {
        
            try{
                $url = env('UPDATE_TRACKING_CONTRATO_VENDOR')."?ids=".$verify_client_tracking['id'];
                $client = new \GuzzleHttp\Client();
                $response = $client->delete($url, ['json' => null,'headers' => [
                    'Content-Type'     => 'application/json',
                    'Authorization'      => env('TOKEN_ZOHO_CRM')
                ]]);
                \Log::info('DELETE_TRACKING_CONTRATO_VENDOR');
            }catch(Exception $e){
                \Log::info('DELETE_TRACKING_CONTRATO_VENDOR_CATCH');
            }

            $data_client_tracking = array(
                'Date' => $date[0],
                'Invoice_number' => $getInvoice->invoice_number,
                'Invoiced' => (string)$verify_client_tracking['data']['new_cant_invoiced'],
                'Dispatch_ID' => $id_dispatch,
                'Vendor_contract_IDs' => $name_contract,
                'Tons' => (string)$verify_client_tracking['data']['new_cant_tons'],
                'URL_1' => $invoice_url
            );

            $tmp_client_tracking_upd = array('id' => $id_contract, 'Tracking_invoices' => [$data_client_tracking]);

            $client_tracking = [
                'data' => [$tmp_client_tracking_upd]
            ];

        }

        //verificacion de historial client en tracking ionvoices
        $history_client = $this->historyData($name_contract,1);//historial de tracking invoices
        if (!empty($history_client) && sizeof($history_client) > 0) {
            $history_client[] = $data_client_tracking;
        }

        if (!empty($history_client) && sizeof($history_client) > 0) {

            $tmp_client_tracking = array('id' => $id_contract, 'Tracking_invoices' => $history_client);

            $client_tracking = [
                'data' => [$tmp_client_tracking]
            ];


            $url = env('INSERTAR_TRACKING_CONTRATO_VENDOR');
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $client_tracking,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);
            \Log::info('actualizar_TRACKING_CONTRATO_CLIENT');
            
        }else{

            $url = env('INSERTAR_TRACKING_CONTRATO_VENDOR');
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $client_tracking,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);
            \Log::info('INSERTAR_TRACKING_CONTRATO_VENDOR');
        }


        $update_client = json_decode($response->getBody());
        \Log::info((array)$update_client);
                        //dd($update_client);
        if (isset($update_client->data[0]->code) && $update_client->data[0]->code == "SUCCESS") {
            \Log::info('Se hace los insert de tracking');

            //operacion de remain balance client contract
            if ($verify_client_tracking['Update']) {
                if ($verify_client_tracking['data']['oper_tons'] == 'sum') {
                    $new_remain_balance = ((int)$contract_client->Remain_balance) + (int)$verify_client_tracking['data']['cant_tons'];
                }elseif ($verify_client_tracking['data']['oper_tons'] == 'res') {
                    $new_remain_balance = ((int)$contract_client->Remain_balance) - (int)$verify_client_tracking['data']['cant_tons'];
                }
                \Log::info($new_remain_balance);
            }else{
                $new_remain_balance = ((int)$contract_client->Remain_balance) - (int)$tons;
                \Log::info($new_remain_balance);
            }

            if ($contract_client->Status == "Not started" && (int)$new_remain_balance < (int)$contract_client->Tons_number) {
                $status = "In process";
            }elseif ((int)$new_remain_balance <= 0) {
                $status = "Completed";
                $finish_date = Carbon::now()->format('Y-m-d');
            }else{
                $status = $contract_client->Status;
            }

            if (isset($finish_date)) {
                $data_client_contract = array(
                        'Remain_balance' => $new_remain_balance,
                        'Status' => $status,
                        'Finish_date' => $finish_date
                    );
            }else{
                $data_client_contract = array(
                        'Remain_balance' => $new_remain_balance,
                        'Status' => $status
                    );
            }
            
            $client_contract = [
                    'data' => [$data_client_contract]
                ];

            $url = env('CRUD_CONTRATO_CLIENT')."/".$contract_client->id;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $client_contract,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);

            $update_contract_client = json_decode($response->getBody());

            if (isset($update_contract_client->data[0]->code) && $update_contract_client->data[0]->code == "SUCCESS") {
                \Log::info('Se actualizan contratos');
                return array('success' => true, 'message' => 'Se actualizo correctamente la informacion del contrato y sus tracking invoices (Sección actualizacion de client contract)');
            }else{
                return array('success' => false, 'message' => 'No se pudo actualiza informacion del contrato (Sección actualizacion de client contract)');
            }

        }else{
            return array('success' => false, 'message' => 'No se pudo actualiza el tracking invoices (Sección actualizacion de client contract)');
        }
    }

    public function updateContractVendor($name_contract,$id_contract,$name_dispatch,$id_dispatch,$factura,$tons,$vendor_contracts,$getInvoice){

        $billed_value = $getInvoice->total; //Valor facturado
        $checked_date = $getInvoice->created_time; //Fecha de facturacion
        $invoice_url = "https://invoice.zoho.com/app#/invoices/".$getInvoice->invoice_id; //Url de la factura
        $summary_invoice_items = $getInvoice->line_items;//Resumen de productos facturados
        $tons = $tons;
        $date = explode("T",$getInvoice->created_time);

        $contract_vendor = $this->getVendorContract($name_contract);

        if (empty($contract_vendor)) {
            return array('success' => false, 'message' => 'No se pudo obtener informacion del contrato (Sección actualizacion de vendor contract)'.$name_contract );
        }

        //validacion de remain balance de inicio de contrato
        if ($contract_vendor->Status == "Not started" && (int)$contract_vendor->Remain_balance == 0 || $contract_vendor->Status == "Not started" && $contract_vendor->Remain_balance == null) {

            $data_vendor_contract_ = array(
                    'Remain_balance' => (int)$contract_vendor->Metrics_tons
                );

            $vendor_contract_ = [
                'data' => [$data_vendor_contract_]
            ];

            $url = env('CRUD_CONTRATO_VENDOR')."/".$contract_vendor->id;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $vendor_contract_,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);

            $update_contract_vendor_ = json_decode($response->getBody());

            $contract_vendor = $this->getVendorContract($name_contract);//Actualizao la informacion

        }

        //Insert o update en tracking invoice vendor
        $data_vendor_tracking = array(
            'Date' => $date[0],
            'Invoice_ID' => $getInvoice->invoice_number,
            'Invoiced' => (string)$billed_value,
            'Dispatchs' => $id_dispatch,
            'Client_contract_IDs' => $vendor_contracts['especial_names'],
            'Tons' => (string)$tons,
            'Invoice_link' => $invoice_url
        );

        $tmp_vendor_tracking = array('id' => $id_contract, 'Tracking_client_invoices' => [$data_vendor_tracking]);

        $vendor_tracking = [
            'data' => [$tmp_vendor_tracking]
        ];

        \Log::info((array)$vendor_tracking);

        $verify_vendor_tracking = $this->isUpdate($data_vendor_tracking,2);

        //Validacion para continuar o finalizar el proceso
        if ($verify_vendor_tracking['isUpdate'] == true && $verify_vendor_tracking['Update'] == false) {
                \Log::info('Es una actualizacion, pero no se encontraron cambios en la factura.');
                $msj = "No se encontraron actualizaciones para la factura ".$getInvoice->invoice_number;
                return $this->jsonResponse(false, "Operación de actualizacion de contrato", $msj,9,null,200); 
        }

        if ($verify_vendor_tracking['Update']) {
        
            try{
                $url = env('UPDATE_TRACKING_CONTRATO_CLIENT')."?ids=".$verify_vendor_tracking['id'];
                $client = new \GuzzleHttp\Client();
                $response = $client->delete($url, ['json' => null,'headers' => [
                    'Content-Type'     => 'application/json',
                    'Authorization'      => env('TOKEN_ZOHO_CRM')
                ]]);
                \Log::info('DELETE_TRACKING_CONTRATO_VENDOR');
            }catch(Exception $e){
                \Log::info('DELETE_TRACKING_CONTRATO_VENDOR_CATCH');
            }

            $data_vendor_tracking = array(
                'Date' => $date[0],
                'Invoice_ID' => $getInvoice->invoice_number,
                'Invoiced' => (string)$verify_vendor_tracking['data']['new_cant_invoiced'],
                'Dispatchs' => $id_dispatch,
                'Client_contract_IDs' => $vendor_contracts['especial_names'],
                'Tons' => (string)$verify_vendor_tracking['data']['new_cant_tons'],
                'Invoice_link' => $invoice_url
            );

            $tmp_vendor_tracking_upd = array('id' => $id_contract, 'Tracking_client_invoices' => [$data_vendor_tracking]);

            $vendor_tracking = [
                'data' => [$tmp_vendor_tracking_upd]
            ];

        }

        //actualiza contratos
        //verificacion de historial client en tracking ionvoices
        $history_vendor = $this->historyData($name_contract,2);//historial de tracking invoices
        if (!empty($history_vendor) && sizeof($history_vendor) > 0) {
            $history_vendor[] = $data_vendor_tracking;
        }

        if (!empty($history_vendor) && sizeof($history_vendor) > 0) {

            $tmp_vendor_tracking = array('id' => $id_contract, 'Tracking_client_invoices' => $history_vendor);

            $vendor_tracking = [
                'data' => [$tmp_vendor_tracking]
            ];


            $url = env('INSERTAR_TRACKING_CONTRATO_CLIENT');
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $vendor_tracking,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);
            \Log::info('actualizar_TRACKING_CONTRATO_VENDOR');
            
        }else{

            $url = env('INSERTAR_TRACKING_CONTRATO_CLIENT');
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $vendor_tracking,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);
            \Log::info('INSERTAR_TRACKING_CONTRATO_VENDOR');
        }

        $update_vendor = json_decode($response->getBody());
        \Log::info((array)$update_vendor);
                        //dd($update_vendor);
        if (isset($update_vendor->data[0]->code) && $update_vendor->data[0]->code == "SUCCESS") {
            \Log::info('Se hace los insert de tracking');

            //operacion de remain balance client contract
            if ($verify_vendor_tracking['Update']) {
                if ($verify_vendor_tracking['data']['oper_tons'] == 'sum') {
                    $new_remain_balance = ((int)$contract_vendor->Remain_balance) + (int)$verify_vendor_tracking['data']['cant_tons'];
                }elseif ($verify_vendor_tracking['data']['oper_tons'] == 'res') {
                    $new_remain_balance = ((int)$contract_vendor->Remain_balance) - (int)$verify_vendor_tracking['data']['cant_tons'];
                }
                \Log::info($new_remain_balance);
            }else{
                $new_remain_balance = ((int)$contract_vendor->Remain_balance) - (int)$tons;
                \Log::info($new_remain_balance);
            }

            if ($contract_vendor->Status == "Not started" && (int)$new_remain_balance < (int)$contract_vendor->Metrics_tons) {
                $status = "In process";
            }elseif ((int)$new_remain_balance <= 0) {
                $status = "Completed";
                $finish_date = Carbon::now()->format('Y-m-d');
            }else{
                $status = $contract_vendor->Status;
            }

            if (isset($finish_date)) {
                $data_vendor_contract = array(
                        'Remain_balance' => $new_remain_balance,
                        'Status' => $status,
                        'Finish_date' => $finish_date
                    );
            }else{
                $data_vendor_contract = array(
                        'Remain_balance' => $new_remain_balance,
                        'Status' => $status
                    );
            }
            
            $vendor_contract = [
                    'data' => [$data_vendor_contract]
                ];

            $url = env('CRUD_CONTRATO_VENDOR')."/".$contract_vendor->id;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', $url, ['json' => $vendor_contract,'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'      => env('TOKEN_ZOHO_CRM')
            ]]);

            $update_contract_vendor = json_decode($response->getBody());

            if (isset($update_contract_vendor->data[0]->code) && $update_contract_vendor->data[0]->code == "SUCCESS") {
                \Log::info('Se actualizan contratos');
                return array('success' => true, 'message' => 'Se actualizo correctamente la informacion del contrato y sus tracking invoices (Sección actualizacion de vendor contract)');
            }else{
                return array('success' => false, 'message' => 'No se pudo actualiza informacion del contrato (Sección actualizacion de vendor contract)');
            }

        }else{
            return array('success' => false, 'message' => 'No se pudo actualiza el tracking invoices (Sección actualizacion de vendor contract)');
        }
    }

    public function getVendorContractsDispatch($id_dispatch){

        $return = array(
                'names_contracts' => [],
                'id_contracts' => [],
                'contracts' => [],
                'especial_ids' => '',
                'especial_names' => '',
                'no_containers' => 0,
                'presentation' => "",
                'account_name' => "",
                'verify_stock' => []
            );

        $url = env('CRUD_TRACKING_DESPACHOS');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM'),
                            'Content-Type' => 'application/json'
                        ]]);
        $trackin_dispatchs = json_decode($request->getBody());

        if (isset($trackin_dispatchs->data) && sizeof($trackin_dispatchs->data) > 0) {

            foreach ($trackin_dispatchs->data as $key => $value) {
                if (!empty($value->Parent_Id) && $value->Parent_Id->id == $id_dispatch) {

                    $return['names_contracts'][] = $value->Contract_vendor->name;
                    $return['id_contracts'][] = $value->Contract_vendor->id;
                    $return['contracts'][] = $value->Contract_vendor;
                    $return['verify_stock'][] = $this->verifyStock($value->Contract_vendor->name);
                }
            }
            
            $return['especial_ids'] = implode(",", $return['id_contracts']);
            $return['especial_names'] = implode(",", $return['names_contracts']);

            return $return;

        }else{
            return null;
        }
    }

    public function verifyStock($name_contract){

        $info = $this->getVendorContract($name_contract);

        if (!empty($info)) {

            if($info->Remain_balance == null || $info->Remain_balance == ""){
                $tons = $info->Metrics_tons;
            }else{
                $tons = $info->Remain_balance;
            }

            return array('id' => $info->id,'name' => $info->Name,'Metrics_tons' => $tons);
        }else{
            return null;
        }

    }

    public function getVendorsDispatchsWarehouse($id_dispatch){

        $contracts = [];
        $url = env('CRUD_TRACKING_DESPACHOS');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM'),
                            'Content-Type' => 'application/json'
                        ]]);
        $trackin_dispatchs = json_decode($request->getBody());

        if (isset($trackin_dispatchs->data) && sizeof($trackin_dispatchs->data) > 0) {
            
            foreach ($trackin_dispatchs->data as $key => $value) {
                if (!empty($value->Parent_Id) && $value->Parent_Id->id == $id_dispatch) {
                    $contracts[] = $value->Contract_vendor;
                }
            }

        }else{
            return null;
        }
    }

    public function generateInvoice(){

        $dispatchs_array = $this->getDispatchs(env('TOKEN_ZOHO_CRM'));
        $dispatchs = [];

        $customers = $this->getContacts(env('TOKEN_ZOHO_INVENTORY'),env('ORGANIZATION_INVOICES'));

        //dd($dispatchs_array);
        if (sizeof($dispatchs_array) > 0) {

            foreach ($dispatchs_array as $key_dispatch => $value_dispatch) {
                if ($value_dispatch->Status == "To invoice") {

                    if (!empty($value_dispatch->ingenuity) && !empty($value_dispatch->ingenuity['verify_stock']) && sizeof($value_dispatch->ingenuity['verify_stock']) > 0) {
                        $sum = 0;
                        foreach ($value_dispatch->ingenuity['verify_stock'] as $key_stock => $value_stock) {
                            $sum += $value_stock['Metrics_tons'];
                        }

                        $value_dispatch->ingenuity['verify_stock']['sum'] = $sum;
                    }

                    $dispatchs[] = $value_dispatch;
                }
            }

            if (sizeof($dispatchs) > 0) {
                $bool_Info = true;
                $dispatchs_json = json_encode($dispatchs);
            }else{
                $bool_Info = false;
                $dispatchs_json = null;
                $object = array(
                                'Type_of_product' => '0',
                                'Metrics_tons' => '0',
                                'info_contract' => (object)array('Selling_price'=> '0'),
                                'ingenuity'=> array('verify_stock' => array('sum'=>""))
                                );
                $dispatchs[0] = (object)$object;
            }
        }else{
            $bool_Info = false;
            $dispatchs_json = null;
            $object = array(
                            'Type_of_product' => '0',
                            'Metrics_tons' => '0',
                            'info_contract' => (object)array('Selling_price'=> '0'),
                            'ingenuity'=> array('verify_stock' => array('sum'=>""))
                            );
            $dispatchs[0] = (object)$object;
        }
        
        //dd($customers,$dispatchs);
        return view('createInvoice')
                ->with('bool_Info',$bool_Info)
                ->with('customers',$customers)
                ->with('dispatchs',$dispatchs)
                ->with('dispatchs_json',$dispatchs_json)
                ->with('messages',null);
    }

    public function getContacts($token,$organization_id){
        $url = env('CONSULTAR_CONTACTOS_INVENTORY')."/?authtoken=".$token."&organization_id=".$organization_id;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url);
        $contacts = json_decode($request->getBody());

        if (isset($contacts->message) && $contacts->message == 'success') {

            return $contacts->contacts;

        }else{
            return null;
        }
    }

    public function getDispatchs($token){
        $url = env('CONSULTAR_DESPACHOS_CRM');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => $token
                        ]]);
        $dispatchs = json_decode($request->getBody());

        if (isset($dispatchs->data) && sizeof($dispatchs->data) > 0) {

            foreach ($dispatchs->data as $key => $value) {
                $info_contract = $this->getClientContract($value->Contract_name->name);
                $info_ingenuity = $this->getVendorContractsDispatch($value->id);
                if (!empty($info_contract)) {
                    $value->ingenuity = $info_ingenuity;
                    $value->info_contract = $info_contract;
                }else{
                    $value->ingenuity = null;
                    $value->info_contract = null;
                }
            }
            
            return array_reverse($dispatchs->data);

        }else{
            return null;
        }
    }

    public function getOneDispatch($dispatch){
        $url = env('CONSULTAR_DESPACHOS_CRM')."/".$dispatch;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
        $dispatchs = json_decode($request->getBody());

        if (isset($dispatchs->data) && sizeof($dispatchs->data) > 0) {
            
            $info_contract = $this->getClientContract($dispatchs->data[0]->Contract_name->name);
            $info_ingenuity = $this->getVendorContractsDispatch($dispatchs->data[0]->id);

            if (!empty($info_contract) && !empty($info_ingenuity)) {

                $sum = 0;

                foreach ($info_ingenuity['verify_stock'] as $key_stock => $value_stock) {
                    $sum += $value_stock['Metrics_tons'];
                }

                $dispatchs->data[0]->ingenuity = $info_ingenuity;
                $dispatchs->data[0]->ingenuity['verify_stock']['sum'] = $sum;
                $dispatchs->data[0]->info_contract = $info_contract;
            }else{
                $dispatchs->data[0]->ingenuity = null;
                $dispatchs->data[0]->info_contract = null;
            }

            return $dispatchs->data;

        }else{
            return null;
        }
    }

    public function insertPayment($getInvoice,$tons,$vendor_contracts,$info,$dispatch,$id_contracts_vendor,$amount_1){

        $date = explode("T",$getInvoice->created_time);

        $Vendor_related_invoices = [];

        foreach ($vendor_contracts['contracts'] as $key_up => $value_up) {

            $Vendor_related_invoices[] = array(
                "Date" => $date[0],
                "Contract" => $value_up->id
                //"No_Invoice" => $getInvoice->invoice_number
                //"Vendor_account" => $id_contracts_vendor[0]
            );

        }

        //Insert o update en tracking invoice vendor

        $tmp_data_payment = array(
            'id' => "3680689000000468013",
            'Client_date_invoice' => $date[0],
            'Name' => $getInvoice->invoice_number,
            'Amount' => $getInvoice->total,
            'Account' => $dispatch[0]->Account_name->id,
            'Containers' => count($dispatch[0]->Containers),
            'Price' => ($amount_1 / $tons),
            'Quantity' => (string)$tons,
            'Presentation' => $dispatch[0]->Presentation,
            'Vendor_related_invoices' => $Vendor_related_invoices,
            'Owner' => $dispatch[0]->Owner->id,
            'Modified_By' => $dispatch[0]->Modified_By->id,
            'Created_By' => $dispatch[0]->Created_By->id,
            'Apply_pre_invoice' => ($getInvoice->custom_field_hash->cf_apply_pre_invoice == "true")? true : false,
            'Amount_pre_invoice' => $getInvoice->custom_fields[3]->value,
            'Note_pre_invoice' => $getInvoice->custom_field_hash->cf_note_pre_invoice
            );

        $payment = [
            'data' => [$tmp_data_payment]
        ];

        $url = env('CRUD_PAYMENTS');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, ['json' => $payment,'headers' => [
            'Content-Type'     => 'application/json',
            'Authorization'      => env('TOKEN_ZOHO_CRM')
        ]]);
        \Log::info('CRUD_PAYMENTS');

        $payment_insert = json_decode($response->getBody());
        \Log::info((array)$payment_insert);

        if (isset($payment_insert->data[0]->code) && $payment_insert->data[0]->code == "SUCCESS") {
            return array('success' => true, 'message' => 'Se inserto correctamente el registro en cartera (Sección añadir registro en payment)');
        }else{
            return array('success' => false, 'message' => 'Ocurrio un error en la insercion de cartera (Sección añadir registro en payment)');
        }
    }

    public function globalOneSettings($updateName,$prefix,$value){
        
        $url = env('CRUD_GLOBAL_SETTINGS');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
        $sequences = json_decode($request->getBody());

        if (isset($sequences->data) && sizeof($sequences->data) > 0) {

            $sequence = [];

            foreach ($sequences->data as $key_seq => $value_seq) {
                if ($value_seq->sequenceName == $updateName) {
                    foreach ($value_seq as $key_ => $value_) {

                        if ($key_ == $prefix || $key_ == $value || $key_ == "id") {
                            $sequence[$key_] = $value_;
                        }
                    }
                }
            }
            

            return $sequence;

        }else{
            return null;
        }
    }

    public function globalUpdateSettings($id,$value,$settings){

        $dataArray = [];
        $url = env('CRUD_GLOBAL_SETTINGS');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url,['headers' => [
                            'Authorization'      => env('TOKEN_ZOHO_CRM')
                        ]]);
        $sequences = json_decode($request->getBody());

        if (isset($sequences) && sizeof($sequences->data) > 0) {
            foreach ($sequences->data as $key => $value_) {

                if ($value_->id == $settings['id']) {


                    $value_->$value = $value_->$value+1;

                    $dataArray[] = $value_; 

                }else{
                    $dataArray[] = $value_; 
                }
            }
        }else{
            return false;
        }

        $actionUpdate = array('data' => [array('id' => '3680689000000590080',"Sequences_modules" => $dataArray)]);

        $url = env('CRUD_GLOBAL_SETTINGS_PARENT');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('PUT', $url, ['json' => $actionUpdate,'headers' => [
            'Content-Type'     => 'application/json',
            'Authorization'      => env('TOKEN_ZOHO_CRM')
        ]]);

        $update_response = json_decode($response->getBody());

        if (isset($update_response->data) && $update_response->data[0]->code == 'SUCCESS') {
            return true;
        }else{
            return false;
        }
    }

    /**
    * Este servicio permite actualizar el contrato con la secuencia actual.
    * 
    * @param string $id Nombre: Nombre o id del contrato, depende de la configuracion del workflow de zoho.
    * @param string $type Nombre: Tipo de operacion a ejecutar, osea si es operacion de contrato con vendor, client, invoice o dispatch.
    * @param string $token Nombre: Token de autenticacion con el api de servicios.
    *
    * @return Single Response
    */
     public function renameContract(Request $request){
        
        $id = $request->get('id');
        $type = $request->get('type');
        $token = $request->get('token');
        
        \Log::info('ingreso a rename contract');
        \Log::info($id);
        \Log::info($type);
        \Log::info($token);
        
        if ($type == 'vendor_contract') {
            $contractToUpdate = $this->getVendorContract($id);
            $updateName = 'contractVendor';
            $url = env('CRUD_CONTRATO_VENDOR');
        }elseif ($type == 'client_contract') {
            $contractToUpdate = $this->getClientContract($id);
            $updateName = 'contractClient';
             $url = env('CRUD_CONTRATO_CLIENT');
        }elseif ($type == 'dispatch') {
            $contractToUpdate = $this->getOneDispatch($id);
            $updateName = 'dispatch';
            $url = env('CONSULTAR_DESPACHOS_CRM');
        }elseif ($type == 'invoice') {
            $contractToUpdate = $this->getInvoices(env('TOKEN_ZOHO_INVENTORY'),env('ORGANIZATION_INVOICES'),$id);

            if (!empty($contractToUpdate)) {
                if ($contractToUpdate->template_id == '1671282000000017001') {
                    $contractToUpdate->Company = 'US';
                }elseif ($contractToUpdate->template_id == '1671282000000283001') {
                    $contractToUpdate->Company = 'EU';
                }
            }
            
            $updateName = 'invoice';
            $url = env('CONSULTAR_FACTURAS');
        }else{
            $msj = "No se recibio un tipo de documento o contrato valido para actualizar.";
            return $this->jsonResponse(false, "Operación de actualizacion de secuencia", $msj,1,null,404);
        }

        if (!empty($contractToUpdate) || sizeof($contractToUpdate) > 0) {

            if ($type == 'dispatch') {
                if ($contractToUpdate[0]->Company == 'US') {
                    $prefix = 'PrefixUS';
                    $value = 'ValueUS';
                    $actionUpdateId = $contractToUpdate[0]->id;
                }elseif($contractToUpdate[0]->Company == 'EU'){
                    $prefix = 'PrefixEU';
                    $value = 'ValueEU';
                    $actionUpdateId = $contractToUpdate[0]->id;
                }
            }elseif($type == 'invoice'){
                if ($contractToUpdate->Company == 'US') {
                    $prefix = 'PrefixUS';
                    $value = 'ValueUS';
                    $actionUpdateId = $contractToUpdate->invoice_id;
                }elseif($contractToUpdate->Company == 'EU'){
                    $prefix = 'PrefixEU';
                    $value = 'ValueEU';
                    $actionUpdateId = $contractToUpdate->invoice_id;
                }
            }else{
                if ($contractToUpdate->Company == 'US') {
                    $prefix = 'PrefixUS';
                    $value = 'ValueUS';
                    $actionUpdateId = $contractToUpdate->id;
                }elseif($contractToUpdate->Company == 'EU'){
                    $prefix = 'PrefixEU';
                    $value = 'ValueEU';
                    $actionUpdateId = $contractToUpdate->id;
                }
            }
            

            $settings = $this->globalOneSettings($updateName,$prefix,$value);

            if (!empty($settings) && sizeof($settings) > 0) {

                $newName = $settings[$prefix].$settings[$value];

                if ($type == 'invoice') {
                    
                    $send_tmp = array(
                        'invoice_number' => $newName
                    );

                    $send = json_encode($send_tmp);

                    $url = env('INVOICES')."/".$actionUpdateId."?authtoken=".env('TOKEN_ZOHO_INVENTORY')."&organization_id=".env('ORGANIZATION_INVOICES');
                    $client = new \GuzzleHttp\Client();
                    $request = $client->request('POST', $url, ['form_params' => ['JSONString' => $send],'headers' => [
                                                    'Content-Type'     => 'application/x-www-form-urlencoded',
                                                    'Authorization'      => env('TOKEN_ZOHO_INVENTORY')
                                                ]]);
                    $invoice = json_decode($request->getBody());

                    if ($invoice->code !== null && $invoice->code == 0) {

                        //Aqui actualizacion de prefijo
                        $updateGlobal = $this->globalUpdateSettings($updateName,$value,$settings);

                        return $this->jsonResponse(true, "Operación de actualizacion de secuencia", $msj,5,null,200);

                    }else{
                        $msj = "Ocurrio un problema con la actualizacion, contacte a su administrador.";
                        return $this->jsonResponse(false, "Operación de actualizacion de secuencia", $msj,4,null,404);
                    }

                }else{

                    $actionUpdate = array('data' => [array('id' => $actionUpdateId,'Name' => $newName)]);
                    //Aqui existe problemas con actualizacion de factura

                    //Update service
                    $client = new \GuzzleHttp\Client();
                    $response = $client->request('PUT', $url, ['json' => $actionUpdate,'headers' => [
                        'Content-Type'     => 'application/json',
                        'Authorization'      => env('TOKEN_ZOHO_CRM')
                    ]]);

                    $update_response = json_decode($response->getBody());

                    if (isset($update_response->data) && $update_response->data[0]->code == 'SUCCESS') {
                        $msj = $update_response->data[0]->message;

                        //Aqui actualizacion de prefijo
                        $updateGlobal = $this->globalUpdateSettings($updateName,$value,$settings);

                        return $this->jsonResponse(true, "Operación de actualizacion de secuencia", $msj,5,null,200);

                    }else{
                        $msj = "Ocurrio un problema con la actualizacion, contacte a su administrador.";
                        return $this->jsonResponse(false, "Operación de actualizacion de secuencia", $msj,4,null,404);
                    }
                }

            }else{
                $msj = "No se encontraron configuraciones de secuencias.";
                return $this->jsonResponse(false, "Operación de actualizacion de secuencia", $msj,3,null,404);
            }

        }else{
            $msj = "No se encontró documento o contrato valido para actualizar.";
            return $this->jsonResponse(false, "Operación de actualizacion de secuencia", $msj,2,null,404);
        }
    }
}
